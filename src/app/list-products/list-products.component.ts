import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../products/product';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  @Input() products: Product[];
  @Input() query: string;

  constructor() { }

  ngOnInit() {
  }

}
