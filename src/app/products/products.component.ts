import { Component, OnInit } from "@angular/core";
import { Product } from "./product";
import { ProductService } from "./product.service";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  products: Product[];
  filterProduct = "";

  getProducts(): void {
    this.productService
      .getProducts()
      .subscribe(products => (this.products = products));
  }

  onFiltered(query: string){
    this.filterProduct = query;
  }

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.getProducts();
  }
}
