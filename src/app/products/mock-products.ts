import { Product } from './product';

export const PRODUCTS: Product[] = [
  { 
    id: 1,
    name: "Конфеты",
    price: 8,
    image: "https://place-hold.it/500x300",
    description: "Вкусно"
  },
  { 
    id: 2,
    name: "Шоколад",
    price: 400,
    image: "https://place-hold.it/500x300",
    description: "Очень вкусно"
  },
  { 
    id: 3,
    name: "Печенье",
    price: 340,
    image: "https://place-hold.it/500x300",
    description: "Максимально"
  },
  { 
    id: 4,
    name: "Мороженое",
    price: 120,
    image: "https://place-hold.it/500x300",
    description: "То се"
  },
  { 
    id: 5,
    name: "Торт",
    price: 248,
    image: "https://place-hold.it/500x300",
    description: "Пятое десятое"
  },
  { 
    id: 6,
    name:"Булочка",
    price: 140,
    image:"https://place-hold.it/500x300",
    description:"Лох"
  },
  { 
    id: 7,
    name: "Ватрушка",
    price: 20,
    image: "https://place-hold.it/500x300",
    description: "Пидр"
  }
];